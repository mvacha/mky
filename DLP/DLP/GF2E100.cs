﻿using System.Collections.Generic;
using System.Numerics;

namespace DLP
{
    public class GF2E100
    {
        public readonly BigInteger Modulus;

        public readonly BigInteger Order = BigInteger.Pow(2, 100);

        public readonly BigInteger MultiplicativeOrder = BigInteger.Pow(2, 100) - 1;

        public Poly Generator => new Poly(this, 2);

        public readonly IList<(int factor, int exp)> OrderFactors =
            new List<(int factor, int exp)> { (3, 1), (5, 3), (11, 1), (31, 1), (41, 1), (101, 1), (251, 1), (601, 1), (1801, 1), (4051, 1), (8101, 1), (268501, 1) };

        public GF2E100(BigInteger modulus) => Modulus = modulus;

    }
}
