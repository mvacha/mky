﻿using System.Diagnostics;
using System.Numerics;

namespace DLP
{
    public static class CoeffArithmetic
    {
        public static BigInteger CoeffPow(BigInteger a, BigInteger b, BigInteger mod)
        {
            Debug.Assert(b >= 0);
            var result = BigInteger.One;
            var expontent = b;
            var baseNumber = a;

            while (!expontent.IsZero)
            {
                if ((expontent & 1) == 1)
                    result = CoeffMul(result, baseNumber, mod);

                expontent >>= 1;
                baseNumber = CoeffMul(baseNumber, baseNumber, mod);
            }

            return result;
        }

        public static BigInteger CoeffMul(BigInteger a, BigInteger b, BigInteger mod)
        {
            var rem = b;
            var currA = a;
            var multResult = BigInteger.Zero;

            while (!rem.IsZero)
            {
                if (rem % 2 == 1)
                    multResult ^= currA;

                currA <<= 1;
                rem >>= 1;
            }

            return CoeffMod(multResult, mod);
        }
        public static BigInteger CoeffMod(BigInteger n, BigInteger mod)
        {
            var aOrigBits = n.NumOfBits();
            var bOrigBits = mod.NumOfBits();

            if (n < mod)
                return n;

            var bitDiff = aOrigBits - bOrigBits;
            var divBy = mod << bitDiff;
            var res = n;
            var divBits = aOrigBits;
            var resBits = aOrigBits;

            Debug.Assert(aOrigBits == divBy.NumOfBits());

            while (true)
            {
                if (resBits == divBits)
                {
                    res ^= divBy;
                    resBits = res.NumOfBits();
                }

                divBits--;
                divBy >>= 1;
                if (divBits < bOrigBits) break;
            }

            return res;
        }
    }
}
