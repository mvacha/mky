﻿using System;
using System.Numerics;
using System.Text;

namespace DLP
{
    public static class Extensions
    {
        public static int NumOfBits(this BigInteger bigInt)
            => bigInt.ToBitString().TrimStart('0').Length;

        //From: https://stackoverflow.com/questions/7483706/c-sharp-modinverse-function
        public static BigInteger ModInverse(this BigInteger a, BigInteger n)
        {
            var i = n;
            var v = BigInteger.Zero;
            var d = BigInteger.One;

            while (a > 0)
            {
                BigInteger t = i / a, x = a;
                a = i % x;
                i = x;
                x = d;
                d = v - t * x;
                v = x;
            }
            v %= n;
            if (v < 0) v = (v + n) % n;
            return v;
        }

        public static string ToBitString(this BigInteger bigInt)
        {
            var coeffBytes = bigInt.ToByteArray();
            var index = coeffBytes.Length - 1;
            var builder = new StringBuilder(coeffBytes.Length * 8);

            while (index >= 0)
            {
                builder.Append(Convert.ToString(coeffBytes[index], 2).PadLeft(8, '0'));
                index -= 1;
            }

            return builder.ToString();
        }
    }
}
