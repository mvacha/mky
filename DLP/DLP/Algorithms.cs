﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace DLP
{
    public static class Algorithms
    {
        public static BigInteger BSGS(this Poly h, Poly g, int n)
        {
            var m = (int)Math.Ceiling(Math.Sqrt(n));
            var bs = new Dictionary<Poly, int>();
            var u = g;
            for (int i = 0; i <= m; i++)
            {
                u = g ^ i;
                bs.Add(u, i);
            }
            u = u ^ -1;
            var y = h;
            var j = 0;
            while (!bs.ContainsKey(y))
            {
                y = y * u;
                j++;
            }

            return bs[y] + m * j;
        }

        //Based on discrete_log in SageMath: https://github.com/sagemath/sagelib/blob/master/sage/groups/generic.py
        public static Poly PohligHellman(this Poly h, Poly g)
        {
            var order = g.Field.MultiplicativeOrder;
            var factors = g.Field.OrderFactors;
            var l = new BigInteger[factors.Count];

            var i = 0;
            foreach ((int factor, int exp) in factors)
            {
                var g0 = g ^ (order / factor);

                for (int j = 0; j < exp; j++)
                {
                    var h0 = (h / (g ^ l[i])) ^ (order / (BigInteger.Pow(factor, (j + 1))));
                    var c = BSGS(h0, g0, factor);
                    l[i] += c * (BigInteger.Pow(factor, j));
                }

                i++;
            }

            var f = factors.Select(m => (BigInteger)Math.Pow(m.factor, m.exp)).ToList();
            var r = CRT(l, f);


            return new Poly(g.Field, r);
        }

        public static BigInteger CRT(IList<BigInteger> aVals, IList<BigInteger> mods)
        {
            var mult = mods.Aggregate((acc, next) => acc * next);

            return mods.
                Select(m => (mult / m).ModInverse(m) * mult / m).
                Zip(aVals, (m, a) => a * m).
                Aggregate((acc, val) => acc + val) % mult;
        }
    }
}
