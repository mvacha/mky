﻿using System;
using System.Numerics;
using System.Text;
using static DLP.CoeffArithmetic;

namespace DLP
{
    public class Poly : IEquatable<Poly>
    {
        public readonly BigInteger Coefficients;
        public readonly GF2E100 Field;

        public Poly(GF2E100 field, BigInteger coefficients)
            => (Field, Coefficients) = (field, coefficients);

        public static Poly operator +(Poly a, Poly b)
            => new Poly(a.Field, a.Coefficients ^ b.Coefficients);

        public static Poly operator -(Poly a, Poly b)
            => a + b;

        public static Poly operator *(Poly a, Poly b)
            => a * b.Coefficients;

        public static Poly operator *(Poly a, BigInteger b)
            => new Poly(a.Field, CoeffMul(a.Coefficients, b, a.Field.Modulus));

        public static Poly operator /(Poly a, Poly b) => a * (b^-1);

        public static Poly operator /(Poly a, BigInteger b)
            => a * (a.Field.MultiplicativeOrder - b);

        public static Poly operator %(Poly a, Poly b)
            => a % b.Coefficients;

        public static Poly operator %(Poly a, BigInteger b)
            => new Poly(a.Field, CoeffMod(a.Coefficients, b));

        public static Poly operator ^(Poly a, Poly b)
            => a ^ b.Coefficients;

        public static Poly operator ^(Poly a, BigInteger b)
        {
            if (b < 0)
                return new Poly(a.Field, CoeffPow(a.Coefficients, a.Field.MultiplicativeOrder + b, a.Field.Modulus));
            else
                return new Poly(a.Field, CoeffPow(a.Coefficients, b, a.Field.Modulus));
        }

        public string ToBitString()
            => Coefficients.ToBitString();

        public string ToPolyString()
        {
            var binStr = ToBitString();
            var exp = binStr.Length - 1;
            var polyStrbuilder = new StringBuilder(binStr.Length * 4);

            foreach (var ch in binStr)
            {
                if (exp == 0 && ch == '1')
                {
                    polyStrbuilder.Append("1");
                    break;
                }

                if (ch == '1')
                    polyStrbuilder.Append($"x^{exp}+");

                exp--;
            }

            return polyStrbuilder.ToString();
        }

        public bool Equals(Poly other) => Coefficients == other.Coefficients;

        public override bool Equals(object obj)
        {
            if (obj is Poly otherPoly)
                return Equals(otherPoly);
            else
                return false;
        }

        public override int GetHashCode() => Coefficients.GetHashCode();
    }
}
