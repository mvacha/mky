﻿using System;
using System.Diagnostics;
using System.Numerics;

namespace DLP
{
    class Program
    {
        static void Main(string[] args)
        {
            var mod = BigInteger.Parse("1267650600228229401496703205765"); //x^100+x^8+x^7+x^2+1
            var gf = new GF2E100(mod);

            //Test addition + mult + div
            var a = new Poly(gf, BigInteger.Parse("70017691339585006900637688970"));
            var b = new Poly(gf, BigInteger.Parse("834687192768531105999675606335"));
            var c = a + b;

            Debug.Assert((a + b).Coefficients == BigInteger.Parse("825464631413057770543832048053"));
            Debug.Assert((a * b).Coefficients == BigInteger.Parse("494566588557535176038980345278"));
            Debug.Assert((a / b).Coefficients == BigInteger.Parse("524579631052834764013390460377"));

            //Test exp
            var exp = new Poly(gf, BigInteger.Parse("422550200076076467165567735125"));
            Debug.Assert((gf.Generator ^ exp).Coefficients == BigInteger.Parse("219356670280504152810857371338"));

            //Inversion test
            var h = new Poly(gf, BigInteger.Parse("1144042772787366463909032478190"));
            Debug.Assert((h ^ new Poly(gf, -1)).Coefficients == BigInteger.Parse("214322394154730158482366299867"));

            //Test Pohlig-Hellman
            var exponent = Algorithms.PohligHellman(h, gf.Generator);
            var isValid = (gf.Generator ^ exponent).Coefficients == h.Coefficients;
            Debug.Assert(exponent.Coefficients == BigInteger.Parse("812273627711493603148574030214"));
            Debug.Assert(isValid);

            Console.WriteLine($"Log_g (h):");
            Console.WriteLine($"Decimal form:{exponent.Coefficients}");
            Console.WriteLine($"Polynomial form:{exponent.ToPolyString()}");
            Console.WriteLine($"Binary form:{exponent.ToBitString()}");
            Console.WriteLine($"IsValid: {isValid}");

            Console.ReadKey();
        }
    }
}
